package jp.hellodea.sample.common.eventbus;

/**
 * Event for the results of startActivityForResult and so on.
 * @author kazuhiro
 *
 */
public class ScreenResultEvent extends BusEvent {

	public ScreenResultEvent(Object eventKey, Object arg) {
		super(eventKey, arg);
	}

    public ScreenResultEvent(Object eventKey) {
        super(eventKey);
    }
}
