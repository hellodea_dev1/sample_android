package jp.hellodea.sample.common.eventbus;

/**
 * Event for exception
 * @author kazuhiro
 *
 */
public class ErrorEvent extends BusEvent {

	public ErrorEvent(Object eventKey) {
		super(eventKey);
	}
	public ErrorEvent(Object eventKey, Exception e) {
		super(eventKey, e);
	}

    public void setException(Exception e){
        setArg(e);
    }

    public Exception getException(){
        return (Exception) getArg();
    }

}
