package jp.hellodea.sample.common;

import android.app.Application;
import android.content.Context;

import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.config.Configuration;

/**
 * Created by kazuhiro on 2015/02/15.
 */
public class JobManagerHolder {
    private static JobManager jobManager;

    public static synchronized void initialize(Application application) {
        if(jobManager == null){
            Configuration configuration = new Configuration.Builder(application).build();
            jobManager = new JobManager(application,configuration);
        }
    }

    private JobManagerHolder() {
    }

    public static JobManager get(){
        return jobManager;
    }
}
