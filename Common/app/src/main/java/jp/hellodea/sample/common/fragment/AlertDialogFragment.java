package jp.hellodea.sample.common.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;

import de.greenrobot.event.EventBus;
import jp.hellodea.sample.common.eventbus.SelectEvent;


@EFragment
public class AlertDialogFragment extends DialogFragment {

	@FragmentArg
    String eventKey;
	
	@FragmentArg
	int iconRes;

    @FragmentArg
    int titleRes;

    @FragmentArg
    int messageRes;

	@FragmentArg
	Integer positiveRes;

	@FragmentArg
	Integer negativeRes;

    @FragmentArg
    Integer neutralRes;

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity())
                .setIcon(iconRes)
                .setTitle(titleRes)
                .setMessage(messageRes);

        if(positiveRes != null){
            builder = builder.setPositiveButton(positiveRes,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            EventBus.getDefault().post(new SelectEvent(eventKey, positiveRes));
                        }
                    }
            );
        }

        if(negativeRes != null){
            builder = builder.setNegativeButton(negativeRes,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            EventBus.getDefault().post(new SelectEvent(eventKey, negativeRes));
                        }
                    }
            );
        }

        if(neutralRes != null){
            builder = builder.setNegativeButton(neutralRes,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            EventBus.getDefault().post(new SelectEvent(eventKey, neutralRes));
                        }
                    }
            );
        }

        return builder.create();
    }
}