package jp.hellodea.sample.common.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class UriUtil {

    /**
     * UriInfo
     */
    public static class UriInfo{
        public String mimeType;
        public String displayName;
        public long size;
    }

    /**
     * get uri info from content provider
     * @param context
     * @param uri
     * @return
     */
    public static UriInfo getUriInfo(Context context, Uri uri){
        UriInfo uriInfo = new UriInfo();

        uriInfo.mimeType = context.getContentResolver().getType(uri);

        Cursor c = context.getContentResolver().query(uri, null, null, null, null);

        c.moveToFirst();
        uriInfo.displayName = c.getString(c.getColumnIndexOrThrow(OpenableColumns.DISPLAY_NAME));
        uriInfo.size = c.getLong(c.getColumnIndexOrThrow(OpenableColumns.SIZE));

        c.close();

        return uriInfo;
    }
}
