package jp.hellodea.sample.common.entity;

import android.content.Context;
import android.text.format.DateUtils;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.Date;

import jp.hellodea.sample.common.AWSConst;

/**
 * Created by kazuhiro on 2015/02/10.
 */
@ParseClassName("Diary")
public class Diary extends ParseObject{
    /**
     * id for find object before saving instead of objectId
     */
    public static final String F_PROXY_ID = "PROXY_ID";

    public static final String F_AUTHOR = "AUTHOR";
    public static final String F_TITLE = "TITLE";
    public static final String F_BODY = "BODY";
    public static final String F_POST_TIME = "POST_TIME";
    public static final String F_PICTURE_ID = "PICTURE_ID";


    /**
     * make image url for the diary
     * @param isThumb
     * @return
     */
    public String getImageUrl(boolean isThumb){
        String url = AWSConst.CLOUD_FRONT + AWSConst.KEY_PREFIX_PICTURE
                + this.getString(F_PICTURE_ID);

        if(isThumb)
            url += AWSConst.KEY_POSTFIX_THUMBNAIL_PICTURE;

        return url;
    }

    /**
     * get post time for show on screen
     * @param context
     * @return
     */
    public String getFormattedPostTime(Context context){

        Date date = this.getDate(Diary.F_POST_TIME);
        if(date == null) return "";

        String timeStr = DateUtils.formatDateTime(
                context, date.getTime(),
                DateUtils.FORMAT_SHOW_YEAR |
                        DateUtils.FORMAT_SHOW_DATE |
                        DateUtils.FORMAT_SHOW_TIME);

        return timeStr;
    }

}
