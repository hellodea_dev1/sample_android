package jp.hellodea.sample.common.entity;

import android.content.Context;
import android.text.format.DateUtils;

import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import jp.hellodea.sample.common.AWSConst;
import jp.hellodea.sample.common.eventbus.CompleteBackgroundEvent;

/**
 * Created by kazuhiro on 2015/02/10.
 */
@ParseClassName("Comment")
public class Comment extends ParseObject{
    public static final String F_DIARY = "DIARY";
    public static final String F_NAME = "NAME";
    public static final String F_COMMENT = "COMMENT";
    public static final String F_POST_TIME = "POST_TIME";


    /**
     * For trial functionality.
     * Create dummy comment data.
     * @param diary
     */
    public static void createDummyData(Diary diary) throws ParseException {
        int max = RandomUtils.nextInt(1,20);
        String characters = "abcdefghij ";
        List<Comment> comments = new ArrayList<Comment>();

        for(int i = 0; i < max; i++){
            Comment comment = new Comment();
            comment.put(Comment.F_DIARY, diary);
            comment.put(Comment.F_NAME, RandomStringUtils.random(20, characters));
            comment.put(Comment.F_COMMENT, RandomStringUtils.random(150, characters));
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH,RandomUtils.nextInt(0,31) * -1);
            comment.put(Comment.F_POST_TIME, calendar.getTime());
            comments.add(comment);
        }

        ParseObject.saveAll(comments);

    }
}
