package jp.hellodea.sample.common.eventbus;

/**
 * Request screen change event
 * @author kazuhiro
 *
 */
public class CallScreenEvent extends BusEvent {

	public CallScreenEvent(Object eventKey, Object arg) {
		super(eventKey, arg);
	}

    public CallScreenEvent(Object eventKey) {
        super(eventKey);
    }
}
