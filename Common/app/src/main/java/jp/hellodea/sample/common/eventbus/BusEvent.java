package jp.hellodea.sample.common.eventbus;

/**
 * General event
 */
public class BusEvent {
	private Object eventKey = null;
	private Object arg = null;

	public BusEvent() {
	}

	public BusEvent(Object eventKey) {
		super();
		this.eventKey = eventKey;
	}

    public BusEvent(Object eventKey, Object arg){
        super();
        this.eventKey = eventKey;
        this.arg = arg;

    }

	public Object getEventKey() {
		return eventKey;
	}

    public void setEventKey(Object eventKey) {
        this.eventKey = eventKey;
    }

    public Object getArg() {
		return arg;
	}

    public void setArg(Object arg) {
        this.arg = arg;
    }

}
