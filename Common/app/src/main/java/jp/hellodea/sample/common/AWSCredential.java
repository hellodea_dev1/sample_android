package jp.hellodea.sample.common;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class AWSCredential {
    public static AWSCredentials getCredentials(){
        return new BasicAWSCredentials(AWSConst.ACCESS_KEY, AWSConst.SECRET_ACCESS_KEY);
    }
}
