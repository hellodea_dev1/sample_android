package jp.hellodea.sample.diary.activity;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.test.ActivityInstrumentationTestCase2;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import jp.hellodea.sample.common.ParseInitializer;
import jp.hellodea.sample.common.entity.Diary;
import jp.hellodea.sample.common.objectgraph.CommonModule;
import jp.hellodea.sample.common.objectgraph.ObjectGraphHolder;
import jp.hellodea.sample.diary.DiaryModule;
import jp.hellodea.sample.diary.R;
import jp.hellodea.sample.diary.fragment.DiaryFragment;
import jp.hellodea.sample.diary.fragment.DiaryFragment_;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.action.ViewActions.*;


/**
 * Created by kazuhiro on 2015/03/17.
 */
@RunWith(AndroidJUnit4.class)
public class DiaryActivityTest extends ActivityInstrumentationTestCase2<DiaryActivity_> {

    private String proxyId;

    public DiaryActivityTest() {
        super(DiaryActivity_.class);
    }

    @BeforeClass
    public static void initWholeTest(){
        Context context = InstrumentationRegistry.getTargetContext();

        //init Parse
        ParseInitializer.initialize(context);

        //init DI modules
        ObjectGraphHolder.plus(new CommonModule((android.app.Application) context.getApplicationContext()));
        ObjectGraphHolder.plus(new DiaryModule());

        return;

    }


    @Before
    @Override
    public void setUp() throws Exception{
        super.setUp();

        injectInstrumentation(InstrumentationRegistry.getInstrumentation());

        Context context = getInstrumentation().getTargetContext();

        //test data
        proxyId = UUID.randomUUID().toString();
        Diary diary = new Diary();
        diary.put(Diary.F_PROXY_ID,proxyId);
        diary.put(Diary.F_TITLE,"test");
        diary.put(Diary.F_BODY,"test body");
        diary.pin();

        //set intent
        Intent intent = new Intent(context,DiaryActivity_.class);
        intent.putExtra("diaryProxyId",proxyId);
        setActivityIntent(intent);

        getActivity();

    }

    @After
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * test for "Open Screen::Show diary detailed information from local cache"
     *
     */
    @Test
    public void testOpenScreen(){

        onView(withId(R.id.textViewTitle)).check(matches(withText("test")));
        onView(withId(R.id.textViewBody)).check(matches(withText("test body")));

    }

    /**
     * test for "Click EDIT::Open edit screen in correction mode"
     */
    @Test
    public void testClickEdit(){
        //click edit button
        onView(withId(R.id.actionEdit)).perform(click());

        //check edit screen is shown
        onView(withId(R.id.editTextTitle)).check(matches(withText("test")));
        onView(withId(R.id.editTextBody)).check(matches(withText("test body")));
    }

}
