package jp.hellodea.sample.diary.logic;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;

import com.amazonaws.mobileconnectors.s3.transfermanager.Transfer;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.UUID;

import hugo.weaving.DebugLog;
import jp.hellodea.sample.common.AWSConst;
import jp.hellodea.sample.common.entity.Comment;
import jp.hellodea.sample.common.entity.Diary;
import jp.hellodea.sample.common.entity.DiaryUser;
import jp.hellodea.sample.common.exception.ErrorCode;
import jp.hellodea.sample.common.exception.LogicException;
import jp.hellodea.sample.common.util.S3Util;
import jp.hellodea.sample.diary.R;
import timber.log.Timber;

/**
 * Created by kazuhiro on 2015/02/10.
 */
public class DiaryLogic {

    /**
     * find all diaries from local datastore
     * @return
     */
    @DebugLog
    public List<Diary> getDiaries() throws LogicException {
        List<Diary> diaries;

        try {
            diaries = ParseQuery.getQuery(Diary.class)
                    .fromLocalDatastore()
                    .orderByDescending(Diary.F_POST_TIME)
                    .find();
        } catch (ParseException e) {
            Timber.e(e,"ParseException %s",e.getMessage());
            throw new LogicException(ErrorCode.CanNotFind,R.string.diary_error_diary_find,e);
        }

        Timber.d("diaries local count:",diaries.size());
        return diaries;
    }

    /**
     * find all diaries from Parse cloud and store in local datastore
     * @return
     * @throws LogicException
     */
    @DebugLog
    public List<Diary> refreshDiaries() throws LogicException {
        List<Diary> diaries;

        try {
            //get all local objects
            diaries = ParseQuery.getQuery(Diary.class)
                    .fromLocalDatastore()
                    .find();

            //unpin all
            ParseObject.unpinAll(diaries);

            //get objects from server
            diaries = ParseQuery.getQuery(Diary.class)
                    .orderByDescending(Diary.F_POST_TIME)
                    .find();

            //pin all
            ParseObject.pinAll(diaries);
        } catch (ParseException e) {
            Timber.e(e, "ParseException %s", e.getMessage());
            if(e.getCode() == ParseException.CONNECTION_FAILED)
                throw new LogicException(ErrorCode.NetworkError,R.string.diary_error_diary_find,e);
            else
                throw new LogicException(ErrorCode.CanNotFind,R.string.diary_error_diary_find,e);
        }

        return diaries;
    }

    /**
     * find a diary from local datastore
     * @param proxyId
     * @return
     */
    @DebugLog
    public Diary getDiary(String proxyId) throws LogicException {
        Diary diary;
        try {
            diary = ParseQuery.getQuery(Diary.class).fromLocalDatastore()
                    .whereEqualTo(Diary.F_PROXY_ID, proxyId).getFirst();
        } catch (ParseException e) {
            Timber.e(e, "ParseException %s", e.getMessage());
            throw new LogicException(ErrorCode.CanNotFind, R.string.diary_error_diary_find, e);
        }

        return diary;
    }

    /**
     * save diary to local
     * @param diary
     */
    @DebugLog
    public void saveDiary(Diary diary) throws LogicException {

        //Save data to local
        try {
            diary.pin();
        } catch (ParseException e) {
            Timber.e(e,"ParseException %s",e.getMessage());
            throw new LogicException(ErrorCode.CanNotSave,R.string.diary_error_diary_post,e);
        }
    }

    /**
     * Upload diary data to Parse server and AWS S3
     * @param proxyId
     * @param imageUriStr
     * @throws LogicException
     */
    @DebugLog
    public void postDiary(Context context, String proxyId, String imageUriStr) throws LogicException{

        Diary diary;
        try {
            //get data from local to post
            diary = ParseQuery.getQuery(Diary.class)
                    .fromLocalDatastore()
                    .whereEqualTo(Diary.F_PROXY_ID,proxyId)
                    .getFirst();
        } catch (ParseException e) {
            Timber.e(e, "ParseException %s", e.getMessage());
            //ignore this exception
            return;
        }

        //send diary to server
        try {
            diary.save();

            //For trial functionality.
            //Create dummy comment data.
            Comment.createDummyData(diary);

        } catch (ParseException e) {
            Timber.e(e, "ParseException %s", e.getMessage());
            throw new LogicException(ErrorCode.CanNotSave,R.string.diary_error_diary_post,e);
        }

        //Upload image data to AWS S3
        if(imageUriStr != null) {
            Uri imageUri = Uri.parse(imageUriStr);

            //post image
            postImage(context,imageUri,1024,1024,
                    AWSConst.KEY_PREFIX_PICTURE + diary.getString(Diary.F_PICTURE_ID),0,70);

            //post thumbnail image
            postImage(context,imageUri,300,300,AWSConst.KEY_PREFIX_PICTURE
                    + diary.getString(Diary.F_PICTURE_ID)
                    + AWSConst.KEY_POSTFIX_THUMBNAIL_PICTURE,71,100);

        }

    }

    @DebugLog
    private void postImage(Context context, Uri imageUri, int width, int height, String fileName, int progressOffset,int maxProgress){
        File file;

        try {
            //resize image file
            FutureTarget<Bitmap> target = Glide.with(context).load(imageUri).asBitmap().into(width,height);
            file = new File(context.getExternalCacheDir(),UUID.randomUUID().toString());
            Bitmap bitmap = target.get();
            OutputStream outputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG,90,outputStream);
            outputStream.close();
            Glide.clear(target);
        } catch (Exception e) {
            Timber.e(e,"exception:%s",e.getMessage());
            throw new RuntimeException(e);
        }

        //upload image to S3
        Transfer.TransferState state = S3Util.uploadFile(AWSConst.BUCKET,
                fileName, "image/jpeg", file, DiaryLogic.class, progressOffset, maxProgress);

        Timber.d("upload status: %s", state.name());

        //delete temp file
        if(file.delete() == false){
            Timber.e("fail to temp file");
            //ignore this error
        };

        if (state.equals(Transfer.TransferState.Failed)) {
            Timber.e("fail to upload s3");
            throw new RuntimeException("s3 upload error");
        }

    }

    /**
     * delete the diary from local and cloud
     * @param diary
     */
    @DebugLog
    public void deleteDiary(Diary diary) throws LogicException{
        try {
            diary.unpin();
            diary.delete();
        } catch (ParseException e) {
            Timber.e(e, "ParseException %s", e.getMessage());
            throw new LogicException(ErrorCode.CanNotDelete, R.string.diary_error_diary_delete, e);
        }

    }

}
