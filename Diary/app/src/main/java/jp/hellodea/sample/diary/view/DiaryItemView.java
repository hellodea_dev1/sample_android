package jp.hellodea.sample.diary.view;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import hugo.weaving.DebugLog;
import jp.hellodea.sample.common.entity.Diary;

/**
 * Created by kazuhiro on 2015/02/12.
 */
@EViewGroup(resName = "item_diary")
public class DiaryItemView  extends LinearLayout{

    @ViewById
    ImageView imageViewPicture;

    @ViewById
    TextView textViewTitle;

    @ViewById
    TextView textViewBody;

    @ViewById
    TextView textViewPostTime;

    public DiaryItemView(Context context) {
        super(context);
    }

    /**
     * bind diary columns to view
     * @param diary
     */
    @DebugLog
    public void bind(Diary diary){
        textViewTitle.setText(diary.getString(Diary.F_TITLE));
        textViewBody.setText(diary.getString(Diary.F_BODY));
        textViewPostTime.setText(diary.getFormattedPostTime(getContext()));

        Glide.with(getContext()).load(diary.getImageUrl(true)).into(imageViewPicture);
    }
}
