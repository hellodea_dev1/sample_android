package jp.hellodea.sample.diary.activity;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.os.Build;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.sample.common.eventbus.CallScreenEvent;
import jp.hellodea.sample.common.eventbus.ScreenResultEvent;
import jp.hellodea.sample.diary.R;
import jp.hellodea.sample.diary.fragment.DiaryListFragment;
import jp.hellodea.sample.diary.fragment.EditFragment_;

@SuppressLint("Registered")
@EActivity(resName = "activity_edit")
public class EditActivity extends ActionBarActivity {

    @Extra
    String diaryProxyId;

    @DebugLog
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            Fragment fragment = EditFragment_.builder().diaryProxyId(diaryProxyId).build();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();

        }
    }

    @DebugLog
    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @DebugLog
    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().registerSticky(this);
    }

    @DebugLog
    @Override
    public void onResume() {
        super.onResume();
    }

    @DebugLog
    @Override
    public void onPause() {
        super.onPause();
    }


    @DebugLog
    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);

    }

    @DebugLog
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @DebugLog
    @Override
    public Intent getSupportParentActivityIntent() {
        Intent intent;
        if(diaryProxyId == null){
            //call diary list
            intent = super.getSupportParentActivityIntent();
        }else{
            //call diary detail
            intent = DiaryActivity_.intent(this).diaryProxyId(diaryProxyId).get();
        }

        return intent;
    }

    /**
     * change new fragment screen
     * @param event
     */
    @DebugLog
    public void onEventMainThread(CallScreenEvent event){
        if(event.getEventKey() == DiaryListFragment.class){
            //Show list screen
            DiaryListActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_CLEAR_TOP).start();

            //remove sticky event after using.
            EventBus.getDefault().removeStickyEvent(event);

        }else if(event.getEventKey() == Intent.ACTION_PICK){
            Intent intent = new Intent();

            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT){
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);
            }else{
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                intent.setType("image/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);
            }

            //Show Picture selector
            startActivityForResult(intent, (Integer) event.getArg());
        }
    }

    @DebugLog
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_OK || data == null) return;

        //post activity result event
        EventBus.getDefault().postSticky(new ScreenResultEvent(requestCode,data));

    }
}
