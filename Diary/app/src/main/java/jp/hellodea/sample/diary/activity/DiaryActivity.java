package jp.hellodea.sample.diary.activity;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

import de.greenrobot.event.EventBus;
import hugo.weaving.DebugLog;
import jp.hellodea.sample.common.eventbus.CallScreenEvent;
import jp.hellodea.sample.diary.R;
import jp.hellodea.sample.diary.fragment.DiaryFragment_;
import jp.hellodea.sample.diary.fragment.DiaryListFragment;
import jp.hellodea.sample.diary.fragment.EditFragment;
import timber.log.Timber;

@SuppressLint("Registered")
@EActivity(resName = "activity_diary")
public class DiaryActivity extends ActionBarActivity {

    @Extra
    String diaryProxyId;

    @DebugLog
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Timber.d("diaryProxyId:%s", diaryProxyId);

        if (savedInstanceState == null) {
            Fragment fragment = DiaryFragment_.builder().diaryProxyId(diaryProxyId).build();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();

        }
    }

    @DebugLog
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @DebugLog
    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @DebugLog
    @Override
    protected void onStart() {
        super.onStart();

        EventBus.getDefault().registerSticky(this);
    }

    @DebugLog
    @Override
    protected void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);
    }

    /**
     * change new fragment screen
     * @param event
     */
    @DebugLog
    public void onEventMainThread(CallScreenEvent event){
        if(event.getEventKey().equals(DiaryListFragment.class)){
            //Show list screen
            DiaryListActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_CLEAR_TOP).start();

            //remove sticky event after using.
            EventBus.getDefault().removeStickyEvent(event);

        }else if(event.getEventKey().equals(EditFragment.class)){
            String id = (String) event.getArg();
            //Show editor screen
            EditActivity_.intent(this).diaryProxyId(id).start();
        }
    }


}
