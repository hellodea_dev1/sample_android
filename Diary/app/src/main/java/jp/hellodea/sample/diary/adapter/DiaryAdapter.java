package jp.hellodea.sample.diary.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import jp.hellodea.sample.common.entity.Diary;
import jp.hellodea.sample.diary.view.DiaryItemView;
import jp.hellodea.sample.diary.view.DiaryItemView_;

/**
 * Created by kazuhiro on 2015/02/12.
 */
public class DiaryAdapter extends ArrayAdapter<Diary> {
    
    public DiaryAdapter(Context context, int resource, List<Diary> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DiaryItemView diaryItemView;
        
        if(convertView == null){
            diaryItemView = DiaryItemView_.build(getContext());
        }else{
            diaryItemView = (DiaryItemView) convertView;
        }

        diaryItemView.bind(getItem(position));

        return diaryItemView;
    }
}
